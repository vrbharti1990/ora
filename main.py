# USAGE
# Start the server:
#   python run_keras_server.py

# import the necessary packages
# import keras
from keras.preprocessing.image import img_to_array
from keras.models import model_from_json
from PIL import Image
import tensorflow as tf
import numpy as np
import flask
from flask import render_template
import time
import io

# initialize our Flask application and the Keras model
app = flask.Flask(__name__)

def load_model(debug):
	# load the pre-trained Keras model (here we are using a model
	# pre-trained on ImageNet and provided by Keras, but you can
	# substitute in your own networks just as easily)
	global model_myDnn, model_resnet50, model_dense201, model_dense169, graph
	st = time.time()
	if debug:
		print("LOADING MODELS...")

	json_file  = open("./models/myDNN/cifar_myDNN.json", "r")
	json_model = json_file.read()
	json_file.close()

	model_myDnn = model_from_json(json_model)
	model_myDnn.load_weights("./models/myDNN/cifar_myDNN_weights.h5")

	if debug:
		print("My DNN Loaded...")

	json_file  = open("./models/resnet50/cifar_resnet50.json", "r")
	json_model = json_file.read()
	json_file.close()

	model_resnet50 = model_from_json(json_model)
	model_resnet50.load_weights("./models/resnet50/cifar_resnet50_weights.h5")
	if debug:
		print("Resnet50 Loaded...")

	json_file  = open("./models/densenet169/cifar_denseNet169.json", "r")
	json_model = json_file.read()
	json_file.close()

	model_dense169 = model_from_json(json_model)
	model_dense169.load_weights("./models/densenet169/cifar_denseNet169_weights.h5")

	if debug:
		print("DenseNet169 Loaded...")

	json_file  = open("./models/densenet201/cifar_denseNet201.json", "r")
	json_model = json_file.read()
	json_file.close()

	model_dense201 = model_from_json(json_model)
	model_dense201.load_weights("./models/densenet201/cifar_denseNet201_weights.h5")
	if debug:
		print("DenseNet201 Loaded...")

	if debug:
		print("MODELS LOADED IN %f secs", time.time()-st)

	graph = tf.get_default_graph()

	return model_myDnn, model_resnet50, model_dense169, model_dense201, graph


# load the models and graph
model_myDnn, model_resnet50, model_dense169, model_dense201, graph = load_model(debug=True)


def prepare_image(image, target):

	# if the image mode is not RGB, convert it
	if image.mode != "RGB":
		image = image.convert("RGB")

	# resize the input image and preprocess it
	image = image.resize(target)
	image = img_to_array(image)
	image = np.expand_dims(image, axis=0)
	image /= 255

	# return the processed image
	return image

@app.route("/")
@app.route("/index.html")
def render_home(appitem=None):
	return render_template("index.html")

@app.route("/app.html")
def load_app():
	return render_template("app.html")  

@app.route("/predict", methods=["POST"])
def predict():

	# ensure an image was properly uploaded to our endpoint
	if flask.request.method == "POST":
		if flask.request.files.get("file"):
			# read the image in PIL format
			image = flask.request.files["file"].read()
			image = Image.open(io.BytesIO(image))

			# preprocess the image and prepare it for classification
			image = prepare_image(image, target=(32, 32))

			# classify the input image and then initialize the list
			# of predictions to return to the client
			global model_myDnn, model_resnet50, model_dense169, model_dense201, graph

			with graph.as_default():
				pred_mydnn  = model_myDnn.predict(image)
				pred_resnet = model_resnet50.predict(image)
				pred_169    = model_dense169.predict(image)
				pred_201    = model_dense201.predict(image)


			# 10 classes
			classes = ["airplane", "automobile", "bird", "cat", "deer", "dog", "frog","horse", "ship", "truck"]

			# Parse the labels
			lab_mydnn  = classes[pred_mydnn.argmax()]
			lab_resnet = classes[pred_resnet.argmax()]
			lab_169    = classes[pred_169.argmax()]
			lab_201    = classes[pred_201.argmax()]

			return_form = '<div class="pt-1 pb-1 pl-1 bg-info text-white">\
						<h2>Predictions</h2>\
						<form>\
						<div class="form-group row" role="form">\
						<label class="col-sm-6 col-form-label">Custom DNN</label>\
						<div class="col-sm-5">\
						<input type="text" class="form-control" id="mydnn_pred" placeholder="'+lab_mydnn+'" disabled>\
						</div>\
						</div>\
						<div class="form-group row">\
						<label class="col-sm-6 col-form-label">ResNet50</label>\
						<div class="col-sm-5">\
						<input type="text" class="form-control" id="resnet" placeholder="'+lab_resnet+'" disabled>\
						</div>\
						</div>\
						<div class="form-group row">\
						<label class="col-sm-6 col-form-label">DenseNet169</label>\
						<div class="col-sm-5">\
						<input type="text" class="form-control" id="dense169" placeholder="'+lab_169+'" disabled>\
						</div>\
						</div>\
						<div class="form-group row">\
						<label class="col-sm-6 col-form-label">DenseNet201</label>\
						<div class="col-sm-5">\
						<input type="text" class="form-control" id="dense201" placeholder="'+lab_201+'" disabled>\
						</div>\
						</div>\
						</form>\
						</div>'
			
	# return the data dictionary as a JSON response
	return return_form


# if this is the main thread of execution first load the model and
# then start the server
if __name__ == "__main__":
	print(("* Loading Keras model and Flask starting server..."
		"please wait until server has fully started"))
	#run the app locally on the givn port
	app.run(host='127.0.0.1', port=8080, debug=True)